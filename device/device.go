package Deviceservice

import (
	"context"
	"fmt"
	"log"

	"github.com/Jerome-0609/microservice/proto/device"
	"github.com/Jerome-0609/microservice/proto/user"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type Device struct {
	serverAddr string
	UserClient user.UserClient
}

//Device constructor
func NewDevice(addr string, con *grpc.ClientConn, grpcServer func(interface{})) *Device {
	// const (
	// 	defserverAddr = "localhost:4040"
	// )
	d := Device{addr, user.NewUserClient(con)}
	grpcServer(&d)
	return &d
}

//Registering Device service to GRpc server
func (dev *Device) RegisterGrpcService(srv *grpc.Server) {
	device.RegisterDeviceServer(srv, dev)
	fmt.Print("device service started on: ")
}

//Returning Device service address
func (dev *Device) GetServerAddr() string {
	return dev.serverAddr
}

//device microservice sends deviceid, userid, username to client on calling FetchData()
func (dev *Device) FetchData(ctx context.Context, req *device.Request) (res *device.Result, err error) {
	devID := req.GetDeviceId()
	if err := contextError(ctx); err != nil {
		return nil, err
	}

	//Device service have deviceid and userid (db/Inmemory)
	//for time being hardcoded
	userid := "123"
	inp := &user.Request{
		UserId: userid,
	}
	username := ""
	if response, err := dev.UserClient.FetchData(context.Background(), inp); err == nil {
		fmt.Println("hit user service")
		username = response.GetUserName()
		fmt.Println("Received username:", username)
	} else {
		return nil, logError(status.Errorf(codes.Internal, "cannot fetch data: %v", err))
	}

	//returning the response
	res = &device.Result{
		DeviceId: devID,
		UserId:   userid,
		UserName: username,
	}
	return res, nil
}

//checking context errors
func contextError(ctx context.Context) error {
	switch ctx.Err() {
	case context.Canceled:
		return logError(status.Error(codes.Canceled, "request is canceled"))
	case context.DeadlineExceeded:
		return logError(status.Error(codes.DeadlineExceeded, "deadline is exceeded"))
	default:
		return nil
	}
}

func logError(err error) error {
	if err != nil {
		log.Print(err)
	}
	return err
}

//starting Grpc server on 4040
// func (dev *Device) Run() {
// 	// listener, err := net.Listen("tcp", ":4040")
// 	// if err != nil {
// 	// 	panic(err)
// 	// }
// 	fmt.Println("device service started")

// 	// srv := grpc.NewServer()
// 	// device.RegisterDeviceServer(srv, dev)
// 	// reflection.Register(srv)

// 	// if e := srv.Serve(listener); e != nil {
// 	// 	panic(e)
// 	// }

// }
