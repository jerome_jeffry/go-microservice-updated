package main

import (
	"context"
	"fmt"

	// "io"
	// "log"
	"github.com/Jerome-0609/microservice/proto/device"

	"google.golang.org/grpc"
)

func main() {
	//calling device micorservice
	conn, err := grpc.Dial("localhost:4040", grpc.WithInsecure())
	if err != nil {
		panic(err)
	}
	client := device.NewDeviceClient(conn)
	//hardcoded for time being
	deviceid := "abcde"

	req := &device.Request{
		DeviceId: deviceid,
	}
	if response, err := client.FetchData(context.Background(), req); err == nil {
		fmt.Println("UserId:", response.GetUserId())
		fmt.Println("UserName:", response.GetUserName())
		fmt.Println("DeviceID", response.GetDeviceId())
	} else {
		fmt.Println(err.Error())
	}

}
